<?php
class Deliveries extends Base
{
    public function get()
    {
        $query = $this->db->prepare("
            SELECT delivery_id, user_id, product_id, delivery_order_date
            FROM deliveries
        ");

        $query->execute();

        return $query->fetchAll();
    }
    public function getById($id)
    {
        $query = $this->db->prepare("
            SELECT
                deliveries.delivery_id,
                deliveries.user_id,
                users.user_name AS customer_name,
                users.user_adress_street AS delivery_address,
                users.user_city AS delivery_city,
                deliveries.delivery_order_date
            FROM
                deliveries
            INNER JOIN
                users USING(user_id)
            WHERE
                deliveries.delivery_id = ?
        ");

        $query->execute([$id]);

        return $query->fetch();
    }
    public function create($data){
        $query = $this->db->prepare("
            INSERT INTO deliveries (user_id,product_id, delivery_address)
            VALUES(?,?,?)
        ");
        $query->execute([
            $data["user_id"],
            $data["product_id"],
            $data["delivery_address"],
        ]);

        return $this->db->lastInsertId();
    }
    public function delete($id) {

        $query = $this->db->prepare("
            DELETE FROM deliveries
            WHERE delivery_id = ?
        ");

        return $query->execute([ $id ]);
    }
}
