<?php
class Products extends Base
{
    public function get()
    {
        $query = $this->db->prepare("
        SELECT products.product_id,products.product_name, products.product_price, products.product_details, products.product_image, categories.category_name 
        FROM categories_products 
        INNER JOIN categories ON categories.category_id = categories_products.category_id
        INNER JOIN products ON products.product_id = categories_products.product_id
        ORDER BY product_image
        ");

        $query->execute();

        $rawProducts = $query->fetchAll();

        $products = [];
        foreach ($rawProducts as $rawProduct) {
            $products[$rawProduct["product_id"]]["product_id"] = $rawProduct["product_id"];
            $products[$rawProduct["product_id"]]["product_name"] = $rawProduct["product_name"];
            $products[$rawProduct["product_id"]]["product_price"] = $rawProduct["product_price"];
            $products[$rawProduct["product_id"]]["product_details"] = $rawProduct["product_details"];
            $products[$rawProduct["product_id"]]["product_image"] = $rawProduct["product_image"];

            $products[$rawProduct["product_id"]]["categories"][] = $rawProduct["category_name"];
        }
        $products = array_values($products);
        return $products;
    }
    public function getById($id)
    {
        $query = $this->db->prepare("
        SELECT products.product_id,products.product_name, products.product_price, products.product_details,products.product_image, categories.category_name 
        FROM categories_products 
        INNER JOIN categories ON categories.category_id = categories_products.category_id
        INNER JOIN products ON products.product_id = categories_products.product_id
        WHERE products.product_id = ?
        ");

        $query->execute([$id]);

        $rawProducts = $query->fetchAll();

        $products = [];
        foreach ($rawProducts as $rawProduct) {
            $products[$rawProduct["product_id"]]["product_id"] = $rawProduct["product_id"];
            $products[$rawProduct["product_id"]]["product_name"] = $rawProduct["product_name"];
            $products[$rawProduct["product_id"]]["product_price"] = $rawProduct["product_price"];
            $products[$rawProduct["product_id"]]["product_details"] = $rawProduct["product_details"];
            $products[$rawProduct["product_id"]]["product_image"] = $rawProduct["product_image"];

            $products[$rawProduct["product_id"]]["categories"][] = $rawProduct["category_name"];
        }

        $products = array_values($products);
        return $products;
    }
    public function create($data)
    {
        
        $binary = base64_decode($data["product_image"]);
        $filename = date("YmdHis") . "_" . mt_rand(10000, 99999) . ".jpg";
        file_put_contents("../uploads/" . $filename, $binary);

        $query = $this->db->prepare("
            INSERT INTO 
                products(product_name,product_details,product_price,product_image)
            VALUES(?,?,?,?)
        ");

        $query->execute([
            $data["product_name"],
            $data["product_details"],
            $data["product_price"],
            $filename
        ]);

        $product_id = $this->db->lastInsertId();

        $query = $this->db->prepare("INSERT INTO categories_products (product_id, category_id) VALUES(?, ?)");

        $query->execute([
            $product_id, $data["category_id"]
        ]);

        $query = $this->db->prepare("INSERT INTO categories_products (product_id, category_id) VALUES(?, ?)");


        $query->execute([
            $product_id, $data["category_id2"]
        ]);

        return $product_id;
    }
    public function update($data)
    {
        $query = $this->db->prepare("
            UPDATE
                products
            SET
                product_name = ?,
                product_price = ?,
                product_details = ?
            WHERE 
                product_id = ?
        ");

        return $query->execute([
            $data["product_name"],
            $data["product_price"],
            $data["product_details"],
            $data["product_id"],
        ]);
    }
    public function delete($id)
    {
        $query = $this->db->prepare("
            DELETE FROM products
            WHERE product_id = ?
        ");

        return $query->execute([$id]);
    }
}
