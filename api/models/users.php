<?php
class Users extends Base
{
    public function get()
    {
        $query = $this->db->prepare("
            SELECT user_id, user_name, user_email
            FROM users
        ");

        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id)
    {
        $query = $this->db->prepare("
            SELECT
                user_id, user_name, user_email, user_adress_street, user_city, user_zipcode, user_password, user_date_of_birth,user_country
            FROM
                users
            WHERE
                user_id = ?
        ");

        $query->execute([$id]);

        return $query->fetch();
    }

    public function create($data) {

        $query = $this->db->prepare("
            INSERT INTO users
            (user_name, user_email, user_adress_street, user_city, user_zipcode, user_password, user_date_of_birth,user_country)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?)
        ");
        
        $query->execute([
            $data["user_name"],
            $data["user_email"],
            $data["user_adress_street"],
            $data["user_city"],
            $data["user_zipcode"],
            password_hash($data["user_password"], PASSWORD_DEFAULT),
            $data["user_date_of_birth"],
            $data["user_country"]
        ]);
        
        return $this->db->lastInsertId();
    }

    public function update($data) {

        $query = $this->db->prepare("
            UPDATE
                users
            SET
                user_name = ?,
                user_email = ?,
                user_adress_street = ?,
                user_city = ?,
                user_zipcode = ?,
                user_password = ?,
                user_date_of_birth = ?,
                user_country = ?
            WHERE
                user_id = ?
        ");

        return $query->execute([
            $data["user_name"],
            $data["user_email"],
            $data["user_adress_street"],
            $data["user_city"],
            $data["user_zipcode"],
            password_hash($data["user_password"], PASSWORD_DEFAULT),
            $data["user_date_of_birth"],
            $data["user_country"],
            $data["user_id"]
        ]);
    }

    public function delete($id) {

        $query = $this->db->prepare("
            DELETE FROM users
            WHERE user_id = ?
        ");

        return $query->execute([ $id ]);
    }

    public function login($data) {
        // login com email e pass
        $query = $this->db->prepare("
            SELECT
                user_id, user_name, user_email,
                user_password, is_admin
            FROM users
            WHERE user_email = ?
        ");
        
        $query->execute([
            $data["user_email"]
        ]);
        
        $user = $query->fetch();

        if(
            !empty($user) &&
            password_verify($data["user_password"], $user["user_password"])
        ) {
            return $user;
        }

        return false;
    }
}
