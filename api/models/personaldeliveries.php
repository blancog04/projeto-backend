<?php
class PersonalDeliveries extends Base
{   
    public function get() {
        $query = $this->db->prepare("
            SELECT * FROM personal_deliveries
        ");
        
        $query->execute();
        
        return $query->fetchAll();
    }
    public function getById($id)
    {
        $query = $this->db->prepare("
        SELECT personal_deliveries.pd_id,personal_deliveries.user_id,users.user_name AS customer_name,users.user_adress_street AS delivery_address,users.user_city AS delivery_city,personal_deliveries.pd_email,personal_deliveries.pd_text,personal_deliveries.pd_image 
        FROM personal_deliveries
        INNER JOIN users USING(user_id)
        WHERE pd_id = ?
        ");

        $query->execute([ $id ]);

        return $query->fetch();
    }
    public function create($data) {

        $query = $this->db->prepare("
            INSERT INTO personal_deliveries
            (pd_id,user_id,pd_email,pd_text,pd_image)
            VALUES(?, ?, ?, ?, ?)
        ");
        
        $query->execute([
            $data["pd_id"],
            $data["user_id"],
            $data["pd_email"],
            $data["pd_text"],
            $data["pd_image"]
        ]);
        
        return $this->db->lastInsertId();
    }
    public function delete($id) {

        $query = $this->db->prepare("
            DELETE FROM personal_deliveries
            WHERE pd_id = ?
        ");

        return $query->execute([ $id ]);
    }
}