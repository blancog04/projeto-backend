<?php
class Categories extends Base
{
    public function get()
    {
        $query = $this->db->prepare("
        SELECT * FROM categories
        ");

        $query->execute();

        return $query->fetchAll();
    }
}