<?php

use ReallySimpleJWT\Token;


class Base
{
    public $db;
    public $user;

    public function __construct()
    {
        $this->db = new PDO(
            "mysql:host=" . ENV["DB_HOST"] . ";dbname=" . ENV["DB_NAME"] . ";charset=utf8mb4",
            ENV["DB_USER"],
            ENV["DB_PASSWORD"]
        );

        $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); // mostra apenas a info que interessa inpedindo de duplicar informação
    }
    public function checkAuthToken()
    {
        $token = "";
        $headers = getallheaders();

        foreach ($headers as $key => $value) {
            if (strtolower($key) === "xauthtoken") {
                $token = $value;
            }
        }

        if (empty($token)) {
            return false;
        }

        try {
            $isValid = Token::validate($token, ENV["JWT_SECRET_KEY"]);
            if ($isValid) {
                $this->user = Token::getPayload($token, ENV["JWT_SECRET_KEY"]);;
            }
        } catch (Exception $ex) {
        }

        return false;
    }
}
