<?php

require("models/users.php");
$model = new Users();

if(in_array($_SERVER["REQUEST_METHOD"], ["PUT", "DELETE"])) {

    $model->checkAuthToken();

    if(
        empty($model->user["user_id"]) ||
        $model->user["user_id"] != $id
    ) {
        http_response_code(403);
        die('{"message":"You do not have permission"}');
    }
}


if ($_SERVER["REQUEST_METHOD"] === "GET") {

    if (!empty($id)) {
        $data = $model->getById($id);

        if (empty($data)) {
            http_response_code(404);
            die('{"message":"Not found"}');
        }
    } else {
        $data = $model->get();
    }

    echo json_encode($data);
} else if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $body = file_get_contents("php://input");
    $data = json_decode($body, true);
    foreach($data as $key => $value) {
        $data[$key] = htmlspecialchars(strip_tags(trim($value)));
    }

    /* efectuar algumas validações mínimas */
    if (
        empty($data) ||
        mb_strlen($data["user_name"]) < 3 ||
        mb_strlen($data["user_name"]) > 40 ||
        mb_strlen($data["user_city"]) < 5 ||
        mb_strlen($data["user_city"]) > 40 ||
        !filter_var($data["user_email"], FILTER_VALIDATE_EMAIL)
    ) {
        http_response_code(400);
        echo '{"message":"Invalid information"}';
        exit;
    }
    
    $data["user_id"] = $model->create($data);

    if (empty($data["user_id"])) {
        http_response_code(500);
        echo '{"message":"Internal error"}';
        exit;
    }

    http_response_code(202);

    unset($data["user_password"]);

    echo json_encode($data);
}
else if( $_SERVER["REQUEST_METHOD"] === "PUT" ) {

    $body = file_get_contents("php://input");
    $data = json_decode($body, true);

    if(
        empty($id) ||
        empty($data) ||
        empty($data["user_name"]) ||
        empty($data["user_email"]) ||
        empty($data["user_password"]) ||
        !filter_var($data["user_email"], FILTER_VALIDATE_EMAIL) ||
        mb_strlen($data["user_name"]) < 3 ||
        mb_strlen($data["user_name"]) > 40 ||
        mb_strlen($data["user_password"]) < 8 ||
        mb_strlen($data["user_password"]) > 1000 ||
        mb_strlen($data["user_adress_street"]) < 8 ||
        mb_strlen($data["user_adress_street"]) > 80 ||
        mb_strlen($data["user_city"]) < 8 ||
        mb_strlen($data["user_city"]) > 80 ||
        mb_strlen($data["user_zipcode"]) < 8 ||
        mb_strlen($data["user_zipcode"]) > 20 ||
        mb_strlen($data["user_country"]) < 8 ||
        mb_strlen($data["user_country"]) > 20 

    ) {
        http_response_code(400);
        echo '{"message":"Invalid information"}';
        exit;
    }

    if(empty( $model->getById($id) )) {
        http_response_code(404);
        die('{"message":"Not found"}');
    }
    
    $data["user_id"] = $id;

    $response = $model->update($data);

    if($response) {
        http_response_code(202);
        unset($data["user_password"]);
        echo json_encode($data);
    }
    else {
        http_response_code(500);
        echo '{"message":"Internal error"}';
    }
}
else if( $_SERVER["REQUEST_METHOD"] === "DELETE" ) {

    if(empty($id)) {
        http_response_code(400);
        echo '{"message":"Invalid information"}';
        exit;
    }
    
    if(empty( $model->getById($id) )) {
        http_response_code(404);
        die('{"message":"Not found"}');
    }

    $response = $model->delete($id);

    if($response) {
        http_response_code(202);
        echo '{"message":"Successfully deleted"}';
    }
    else {
        http_response_code(500);
        echo '{"message":"Internal error"}';
    }
}
else {
    http_response_code(405);
    echo '{"message":"Method not allowed"}';
}
