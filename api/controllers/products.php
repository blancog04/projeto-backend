<?php

require("models/products.php");
$model = new Products();

if(in_array($_SERVER["REQUEST_METHOD"], ["POST", "PUT", "DELETE"])) {
    $model->checkAuthToken();

    if(
        empty($model->user["user_id"]) ||
        empty($model->user["is_admin"])
    ) {

        http_response_code(403);
        die('{"message":"You do not have permission"}');
    }
}

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    if (!empty($id)) {
        $data = $model->getById($id);

        if (empty($data)) {
            http_response_code(404);
            die('{"message":"Not found"}');
        }
    } else {
        $data = $model->get();
    }

    echo json_encode($data);
} else if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $body = file_get_contents("php://input");
    $data = json_decode($body, true);
    foreach($data as $key => $value) {
        $data[$key] = htmlspecialchars(strip_tags(trim($value)));
    }
    if (
        empty($data) ||
        mb_strlen($data["product_name"]) < 3 ||
        mb_strlen($data["product_name"]) > 120 ||
        mb_strlen($data["product_details"]) < 5 ||
        mb_strlen($data["product_details"]) > 65535 ||
        !is_numeric($data["product_price"]) ||
        $data["product_price"] <= 0 ||
        $data["product_price"] < 1
    ) {
        http_response_code(400);
        echo '{"message":"Invalid information"}';
        exit;
    }

    
    
    $data["product_id"] = $model->create($data);

    if (empty($data["product_id"])) {
        http_response_code(500);
        echo '{"message":"Internal error"}';
        exit;
    }

    http_response_code(202);

    echo json_encode($data);
} else if ($_SERVER["REQUEST_METHOD"] === "PUT") {
    $body = file_get_contents("php://input");
    $data = json_decode($body, true);


    if (
        empty($id) ||
        empty($data) ||
        mb_strlen($data["product_name"]) < 3 ||
        mb_strlen($data["product_name"]) > 120 ||
        mb_strlen($data["product_details"]) < 10 ||
        mb_strlen($data["product_details"]) > 65535 ||
        !is_numeric($data["product_price"]) ||
        $data["product_price"] <= 0 ||
        $data["product_price"] < 1
    ) {
        http_response_code(400);
        echo '{"message":"Invalid information"}';
        exit;
    }

    if (empty($model->getById($id))) {
        http_response_code(404);
        die('{"message":"Not found"}');
    }

    $data["product_id"] = $id;

    $response = $model->update($data);

    if ($response) {
        http_response_code(202);
        echo json_encode($data);
    } else {
        http_response_code(500);
        echo '{"message":"Internal error"}';
    }
} else if ($_SERVER["REQUEST_METHOD"] === "DELETE") {

    if (empty($id)) {
        http_response_code(400);
        echo '{"message":"Invalid information"}';
        exit;
    }

    if (empty($model->getById($id))) {
        http_response_code(404);
        die('{"message":"Not found"}');
    }

    $response = $model->delete($id);

    if ($response) {
        http_response_code(202);
        echo '{"message":"Successfully deleted"}';
    } else {
        http_response_code(500);
        echo '{"message":"Internal error"}';
    }
} else {
    http_response_code(405);
    echo '{"message":"Method not allowed"}';
}
