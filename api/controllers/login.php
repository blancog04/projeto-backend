<?php

use ReallySimpleJWT\Token;

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $body = file_get_contents("php://input");
    $data = json_decode($body, true);
    
    if (
        empty($data) ||
        !filter_var($data["user_email"], FILTER_VALIDATE_EMAIL) ||
        mb_strlen($data["user_password"]) < 7 ||
        mb_strlen($data["user_password"]) > 255
    ) {
        http_response_code(400);
        die('{"message":"Bad request"}');
    }
    require("models/users.php");
    $model = new Users();

    $user = $model->login($data);

    if (empty($user)) {
        http_response_code(401);
        die('{"message":"Authentication failed"}');
    }
    /* gerar o JWT */
    $payload = [
        'iat' => time(),
        'exp' => time() + (60 * 60 * 24 * 90),
        'user_id' => $user["user_id"],
        'user_email' => $user["user_email"],
        'user_name' => $user["user_name"],
        'is_admin' => $user["is_admin"]
    ];
    $secret = ENV["JWT_SECRET_KEY"];
    $token = Token::customPayload($payload, $secret);

    header("XAuthToken: " . $token);
    echo '{"XAuthToken":"' .$token. '"}';
}
else {
    http_response_code(405);
    echo '{"message":"Method not allowed"}';
}