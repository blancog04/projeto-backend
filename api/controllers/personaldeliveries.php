<?php

require("models/personaldeliveries.php");
$model = new PersonalDeliveries();

if(in_array($_SERVER["REQUEST_METHOD"], ["GET","DELETE"])) {
    //$model->checkApiKey();
    $model->checkAuthToken();

    if(
        empty($model->user["user_id"]) ||
        empty($model->user["is_admin"])
    ) {
        http_response_code(403);
        die('{"message":"You do not have permission"}');
    }
}

if( $_SERVER["REQUEST_METHOD"] === "GET" ) {
    
    if(!empty($id)) {
        $data = $model->getById($id);

        if(empty($data)) {
            http_response_code(404);
            die('{"message":"Not found"}');
        }
    }
    else {
        $data = $model->get();
    }

    echo json_encode($data);

}
else if( $_SERVER["REQUEST_METHOD"] === "POST" ) {
    $body = file_get_contents("php://input");
    $data = json_decode($body, true);
    foreach($data as $key => $value) {
        $data[$key] = htmlspecialchars(strip_tags(trim($value)));
    }
    if(
        empty($data) ||
        !filter_var($data["pd_email"], FILTER_VALIDATE_EMAIL ||
        mb_strlen($data["pd_text"]) < 20 ||
        mb_strlen($data["pd_text"]) > 200 )
    ) {
        http_response_code(400);
        echo '{"message":"Invalid information"}';
        exit;
    }
    foreach($data as $key => $value) {
        $data[$key] = htmlspecialchars(strip_tags(trim($value)));
    }
    $data["pd_id"] = $model->create($data);

    if(empty($data["pd_id"])) {
        http_response_code(500);
        echo '{"message":"Internal error"}';
        exit;
    }

    http_response_code(202);

    echo json_encode($data);
}
else if( $_SERVER["REQUEST_METHOD"] === "DELETE" ) {

    if(empty($id)) {
        http_response_code(400);
        echo '{"message":"Invalid information"}';
        exit;
    }
    
    if(empty( $model->getById($id) )) {
        http_response_code(404);
        die('{"message":"Not found"}');
    }

    $response = $model->delete($id);

    if($response) {
        http_response_code(202);
        echo '{"message":"Successfully deleted"}';
    }
    else {
        http_response_code(500);
        echo '{"message":"Internal error"}';
    }
}
else {
    http_response_code(405);
    echo '{"message":"Method not allowed"}';
}