<?php
require("models/categories.php");
$model = new Categories();

if( $_SERVER["REQUEST_METHOD"] === "GET" ) {
    $data = $model->get();

    echo json_encode($data);
}
else {
    http_response_code(405);
    echo '{"message":"Method not allowed"}';
}