<?php
require("vendor/autoload.php");

header("Content-Type: application/json");

define("ENV", parse_ini_file(".env"));

$parts = explode("/", $_SERVER["REQUEST_URI"]);

$resource = $parts[2];

$id = $parts[3] ?? "";

$controllers = ["products", "deliveries", "users", "login", "personaldeliveries", "categories"]; // whitelist

if (!in_array($resource, $controllers)) {
    http_response_code(400);
    die('{"message": "Bad Request"}');
}

require_once("models/base.php");

require("controllers/" . $resource . ".php");